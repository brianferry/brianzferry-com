const Metalsmith = require('metalsmith')
const markdown = require('metalsmith-markdown')
const layouts = require('metalsmith-layouts')
const permalinks = require('metalsmith-permalinks')
const collections = require('metalsmith-collections')
const dateFormatter = require('metalsmith-date-formatter')
const pagination = require('metalsmith-pagination')
const handlebars = require('handlebars')

handlebars.registerHelper('safeString', function (content) {
  return new handlebars.SafeString(content);
})

Metalsmith(__dirname)
  .metadata({
    navigation_title: "Brian Ferry's Website",
    author_name: "Brian Ferry",
    author_blurb: "Software Engineer | Raleigh, NC",
    description: "Software Engineer living in my home state of North Carolina.",
    generator: "Metalsmith",
    generator_url: "https://brianzferry.com"
  })
  .use(collections({
    "posts": {
      pattern: "posts/*.md",
      sortBy: "date",
      reverse: true
    }
  }))
  .source('./src')
  .destination('./build')
  .clean(false)
  .use(pagination({
    'collections.posts': {
      perPage: 5,
      layout: "home.html",
      first: "index.html",
      path: 'posts/:num/index.html',
      filter: function (page) {
        return !page.private
      }
    }
  }))
  .use(dateFormatter({
    dates: [
      {
        key: 'date',
        format: "MMMM DD, YYYY"
      }
    ]
  }))
  .use(markdown())
  .use(permalinks({
    relative: false
  }))
  .use(layouts({
    engine: 'handlebars',
    partials: 'partials'
  }))
  .build(function (err) {
    if (err) { throw err }
  })