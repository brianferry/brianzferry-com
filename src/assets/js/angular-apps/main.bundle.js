webpackJsonp(["main"],{

    /***/ "./src/$$_lazy_route_resource lazy recursive":
    /***/ (function(module, exports) {
    
    function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncatched exception popping up in devtools
        return Promise.resolve().then(function() {
            throw new Error("Cannot find module '" + req + "'.");
        });
    }
    webpackEmptyAsyncContext.keys = function() { return []; };
    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
    
    /***/ }),
    
    /***/ "./src/app/app.component.css":
    /***/ (function(module, exports) {
    
    module.exports = "\r\n@import 'https://js.arcgis.com/4.6/esri/css/main.css';\n/* import the required JSAPI css */\n#mapDiv {\r\n  padding: 0;\r\n  margin: 0;\r\n  height: 100%;\r\n  width: 100%;\r\n}\n#resultsContainer{\r\n  height:50vw;\r\n  overflow:scroll;\r\n  min-width:300px;\r\n  font-family: 'Lato';\r\n  padding: 5px;\r\n  z-index:1;\r\n  background: #fff;\r\n  color: #444;\r\n}\n#searchContainer{\r\n  height:50px;\r\n  padding-left:5px;\r\n  padding-right:5px;\r\n  min-width:300px;\r\n  min-width:15vw;\r\n  z-index:2;\r\n  font-family: \"Lato\";\r\n  background-color: #fff;\r\n}\n.card{\r\n  padding:5px;\r\n  cursor: pointer;\r\n  margin-top:5px;\r\n  margin-bottom:5px;\r\n}"
    
    /***/ }),
    
    /***/ "./src/app/app.component.html":
    /***/ (function(module, exports) {
    
    module.exports = "<div id=\"searchContainer\"></div>\r\n<div #mapViewNode id=\"mapDiv\">\r\n</div>\r\n\r\n<div id=\"resultsContainer\" [hidden]=\"!showResults\">\r\n    <div class=\"toggleSwitch\">\r\n        <label class=\"custom-control custom-checkbox\">\r\n            <input type=\"checkbox\" class=\"custom-control-input\" [(ngModel)]=\"showProposed\">\r\n            <span class=\"custom-control-indicator\"></span>Show Proposed Structures\r\n        </label>\r\n    </div>\r\n    <div class=\"container\">\r\n        <input type=\"range\" min=\"1\" max=\"5\" value=\"3\" class=\"slider\" (change)=\"sliderChange()\" id=\"myRange\" [(ngModel)]=\"sliderValue\">\r\n        {{ sliderValue }} miles\r\n        <div class=\"container-header\">\r\n            <h4>Existing Structures</h4>\r\n        </div>\r\n        <div>\r\n            <div (click)=\"zoomTo(result)\" class=\"card\" *ngFor=\"let result of (results.features | existing: 'Existing')  | slice:0:5; let i=index\">\r\n                <div class=\"card-header\">\r\n                    {{ result.attributes.NAME != null && result.attributes.NAME.length > 0 ? result.attributes.NAME : 'No Name Found' }}\r\n                </div>\r\n                <ul class=\"list-group list-group-flush\">\r\n                    <li class=\"list-group-item\">\r\n                        <div>{{ result.attributes.FAC }}</div>\r\n                        <div>{{ (result.distance).toPrecision(2) }} miles away</div>\r\n                        <div>{{ (result.attributes['SHAPE.LEN'] / 5280).toPrecision(2) }} miles long</div>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"container\" *ngIf=\"showProposed\">\r\n        <div class=\"container-header\">\r\n            <h4>Proposed Structures</h4>\r\n        </div>\r\n        <div>\r\n            <div>\r\n                <div (click)=\"zoomTo(result)\" *ngFor=\"let result of (results.features | existing: 'Proposed')  | slice:0:5; let i=index\" class=\"card\">\r\n                    <div class=\"card-header\">\r\n                        {{ result.attributes.NAME != null && result.attributes.NAME.length > 0 ? result.attributes.NAME : 'No Name Found' }}\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <div>Type: {{ result.attributes.FAC }}</div>\r\n                        <div>{{ (result.distance).toPrecision(3) }} miles away</div>\r\n                        <div>{{ (result.attributes['SHAPE.LEN'] / 5280).toPrecision(3) }} miles long</div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n\r\n</div>"
    
    /***/ }),
    
    /***/ "./src/app/app.component.ts":
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ExistingPipe; });
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_esri_loader__ = __webpack_require__("./node_modules/esri-loader/dist/umd/esri-loader.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_esri_loader___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_esri_loader__);
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    
    
    var ExistingPipe = /** @class */ (function () {
        function ExistingPipe() {
        }
        ExistingPipe.prototype.transform = function (allResults, status, statusAlternative) {
            if (allResults == null) {
                return;
            }
            return allResults.filter(function (result) { return (result.attributes.STATUS == status || result.attributes.STATUS == statusAlternative) && (result.attributes.NAME != null && result.attributes.NAME.trim().length > 0); });
        };
        ExistingPipe = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Pipe */])({ name: 'existing' })
        ], ExistingPipe);
        return ExistingPipe;
    }());
    
    var AppComponent = /** @class */ (function () {
        function AppComponent() {
            this.rangeChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */]();
            this._zoom = 10;
            this._center = [-78.6382, 35.7796];
            this._basemap = 'streets';
            this.showResults = false;
            this.sliderValue = 3;
            this.showProposed = false;
            this.results = [];
            this.mapLoaded = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */]();
        }
        AppComponent.prototype.zoomTo = function (_map) {
            this._map.goTo(_map.geometry.extent);
            this._resultsLayer.removeAll();
            var features = this._graphic = {
                symbol: {
                    type: "simple-line",
                    outline: {
                        width: 4,
                        color: [12, 32, 255]
                    }
                },
                geometry: _map.geometry
            };
            this._resultsLayer.add(features);
        };
        Object.defineProperty(AppComponent.prototype, "zoom", {
            get: function () {
                return this._zoom;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppComponent.prototype, "center", {
            get: function () {
                return this._center;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppComponent.prototype, "basemap", {
            get: function () {
                return this._basemap;
            },
            enumerable: true,
            configurable: true
        });
        AppComponent.prototype.HideResults = function () {
            //this._map.ui.empty("resultsContainer");
            this.showResults = false;
        };
        AppComponent.prototype.sliderChange = function () {
            this.rangeChange.emit(this.sliderValue);
        };
        AppComponent.prototype.ngOnInit = function () {
            var _this = this;
            Object(__WEBPACK_IMPORTED_MODULE_1_esri_loader__["loadModules"])([
                'esri/Map',
                'esri/views/MapView',
                'esri/layers/FeatureLayer',
                'esri/widgets/Search',
                'esri/geometry/geometryEngine',
                'esri/Graphic',
                'esri/layers/GraphicsLayer',
                'esri/symbols/SimpleFillSymbol',
                'esri/widgets/Directions',
                'esri/core/urlUtils',
                'dojo/query',
                'esri/widgets/Zoom',
                'dojo/on',
                'dojo/dom',
                'dojo/domReady!'
            ])
                .then(function (_a) {
                var EsriMap = _a[0], EsriMapView = _a[1], FeatureLayer = _a[2], Search = _a[3], geometryEngine = _a[4], Graphic = _a[5], GraphicsLayer = _a[6], SimpleFillSymbol = _a[7], Directions = _a[8], urlUtils = _a[9], query = _a[10], Zoom = _a[11], on = _a[12], dom = _a[13];
                var self = _this;
                var baseMapForAllGreenways = "//maps.wakegov.com/arcgis/rest/services/OpenSpace/Greenways/MapServer/0";
                // Set type for Map constructor properties
                var mapProperties = {
                    basemap: _this._basemap
                };
                // var _bufferDistance = "2500";
                var _trailBuffer;
                var _resultsLayers = new GraphicsLayer();
                self._resultsLayer = _resultsLayers;
                self._graphic = Graphic;
                var lastSearchResult;
                var _geometry;
                var selfPoint;
                var map = new EsriMap(mapProperties);
                // Set type for MapView constructor properties
                var mapViewProperties = {
                    container: _this.mapViewEl.nativeElement,
                    center: _this._center,
                    zoom: _this._zoom,
                    map: map
                };
                var flAllGreenways = new FeatureLayer({
                    url: baseMapForAllGreenways,
                    outFields: ["*"],
                    visible: false
                });
                map.layers.add(flAllGreenways);
                map.layers.add(_resultsLayers);
                var mapView = new EsriMapView(mapViewProperties);
                _this._map = mapView;
                var search = new Search({
                    view: mapView,
                    container: "searchContainer"
                });
                var zoom = new Zoom({
                    view: mapView
                });
                mapView.ui.add(search, {
                    position: "top-left",
                    index: 5
                });
                mapView.ui.remove('zoom');
                mapView.ui.add(zoom, {
                    position: "bottom-right",
                    index: 0
                });
                var resultsContainerDiv = document.getElementById('resultsContainer');
                mapView.ui.add(resultsContainerDiv, {
                    position: "top-left",
                    index: 90
                });
                on(search, 'select-result', searchResult);
                self.rangeChange.subscribe(function (data) {
                    sliderChange();
                });
                function sliderChange() {
                    searchResult(lastSearchResult);
                }
                function searchResult(data) {
                    lastSearchResult = data;
                    self.showResults = false;
                    var value = self.sliderValue * 5280;
                    _geometry = data.result.feature.geometry;
                    selfPoint = data.result.feature;
                    _trailBuffer = geometryEngine.geodesicBuffer(_geometry, [
                        value
                    ], "feet", true);
                    queryTrails()
                        .then(function (data) {
                        console.log(data.features);
                        displayResults(data);
                    });
                }
                function queryTrails() {
                    var query = flAllGreenways.createQuery();
                    query.geometry = _trailBuffer;
                    query.spatialRelationship = "intersects";
                    return flAllGreenways.queryFeatures(query);
                }
                function displayResults(result) {
                    self.results = result;
                    self.results.features.forEach(function (item) {
                        var tempBuffer = geometryEngine.distance(item.geometry, selfPoint.geometry, 'miles');
                        item.distance = tempBuffer;
                    });
                    if (self.results != null && self.results.features.length > 0) {
                        self.showResults = true;
                    }
                }
                function updateTrails(evt) {
                    var trailQuery = query("input[id^='chk-']");
                    var _definitionExpression = "";
                    trailQuery.forEach(function (element) {
                        if (element.checked) {
                            _definitionExpression += _definitionExpression.length > 0 ? " OR STATUSFAC = '" + element.value + "'" : "STATUSFAC = '" + element.value + "'";
                        }
                    });
                    flAllGreenways.definitionExpression = _definitionExpression;
                }
                mapView.when(function () {
                    // All the resources in the MapView and the map have loaded. Now execute additional processes
                    _this.mapLoaded.emit(true);
                }, function (err) {
                    console.error(err);
                });
            })
                .catch(function (err) {
                console.error(err);
            });
        }; // ngOnInit
        __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
            __metadata("design:type", Object)
        ], AppComponent.prototype, "rangeChange", void 0);
        __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
            __metadata("design:type", Object)
        ], AppComponent.prototype, "mapLoaded", void 0);
        __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('mapViewNode'),
            __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* ElementRef */])
        ], AppComponent.prototype, "mapViewEl", void 0);
        AppComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
                selector: 'app-root',
                template: __webpack_require__("./src/app/app.component.html"),
                styles: [__webpack_require__("./src/app/app.component.css")]
            }),
            __metadata("design:paramtypes", [])
        ], AppComponent);
        return AppComponent;
    }());
    
    
    
    /***/ }),
    
    /***/ "./src/app/app.module.ts":
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__("./src/app/app.component.ts");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("./src/app/app.component.ts");
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    
    
    
    
    
    var AppModule = /** @class */ (function () {
        function AppModule() {
        }
        AppModule = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* NgModule */])({
                declarations: [
                    __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                    __WEBPACK_IMPORTED_MODULE_2__app_app_component__["b" /* ExistingPipe */]
                ],
                imports: [
                    __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */]
                ],
                providers: [],
                bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
            })
        ], AppModule);
        return AppModule;
    }());
    
    
    
    /***/ }),
    
    /***/ "./src/environments/environment.ts":
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
    // The file contents for the current environment will overwrite these during build.
    // The build system defaults to the dev environment which uses `environment.ts`, but if you do
    // `ng build --env=prod` then `environment.prod.ts` will be used instead.
    // The list of which env maps to which file can be found in `.angular-cli.json`.
    var environment = {
        production: false
    };
    
    
    /***/ }),
    
    /***/ "./src/main.ts":
    /***/ (function(module, __webpack_exports__, __webpack_require__) {
    
    "use strict";
    Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
    
    
    
    
    if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* enableProdMode */])();
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
        .catch(function (err) { return console.log(err); });
    
    
    /***/ }),
    
    /***/ 0:
    /***/ (function(module, exports, __webpack_require__) {
    
    module.exports = __webpack_require__("./src/main.ts");
    
    
    /***/ })
    
    },[0]);
    //# sourceMappingURL=main.bundle.js.map