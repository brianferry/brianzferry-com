---
title: 'Wake County Maps Project'
date: "2019-03-06"
banner_image: "/assets/images/Downtown-Raleigh-from-Western-Boulevard-Overpass-20081012.jpeg"
snippet: "Learn about making a map with this tutorial"
author_name: "Brian Ferry"
layout: post.html
tags: ['Technology']
---
Link to project: <a href="/posts/wake-county-greenway-trail-search" target="_blank">Wake County Trail Search</a>

You can type in an address in or around wake county and you can search for any existing / proposed greenways within 1-5 miles from the place you searched.

The map service has been provided from <a href="https://data-wake.opendata.arcgis.com/" target="_blank">Wake County Open Data</a>

This was a project of my own that I started in order to get used to the ESRI JavaScript 4.0 framework and Angular 5.