---
title: "WebAssembly and the Flappy Bird Blazor Game"
date: "2020-02-20"
banner_image: "/assets/images/blazor-client-server.jpg"
snippet: "The Future of Web Development with WebAssembly"
author_name: "Brian Ferry"
layout: post.html
tags: ["Technology"]
---

<p>
I've always had a love / hate relationship with JavaScript.  On one hand, the language is lightweight, well supported, and can be fit to fill an immeasurable number of use cases.
</p>
<p>
But for all it's pluses, it's always come at a cost.  Slowness, bloat, compatibility issues and oddities with the language itself (see: decimals).  There are a lot of libraries and tools built on top of JavaScript over the years to take care of these issues, but what if we didn't have to?  
</p>
<p>
That's where WebAssembly comes in.  Rather than go over WebAssembly (<a href="https://medium.com/mozilla-tech/why-webassembly-is-a-game-changer-for-the-web-and-a-source-of-pride-for-mozilla-and-firefox-dda80e4c43cb" target="_blank">there's plenty of other article online about this very topic</a>) I'm going to be outlining a small setup project I worked on after going to a <a href="https://dotnet.microsoft.com/apps/aspnet/web-apps/blazor" target="_blank">Blazor</a> Workshop Meetup.  
</p>
<p>
Namely, I wanted to test three main points that I heard from the Meetup that stuck out to me:
</p>
<ol>
<li>That the Blazor code can be hosted on a non-IIS machine</li>
<li>That the Blazor code runs as .dlls loaded into the browser similarly to how JavaScript files are loaded</li>
<li>The Blazor component could be placed within a HTML page similarly to how JavaScript components can be embedded</li>
</ol>

<p>
To do this, I decided to try to take a game ported into Blazor and put it in my website hosted on a static site host (Netlify) in an HTML page.  The repo for this game can be found at <a href="https://github.com/ctrl-alt-d/FlappyBlazorBird" target="_blank">FlappyBlazorBird</a>.  
</p>

<p>
So I then took my "app" tag and tried putting it into my page and ran it and was hit with a 400 error stating that it couldn't find anything at this location, which when I realized I would need to remove the "@page" tag from the index.razor page.  I ended up taking the code itself and moving it into the App.razor page. 
</p>

<p>
Next, since this needs to be able to be embedded into my website and not as a standalone page I will need to alter much of the site.css file.  Removing a lot of the outer div styling, the exact positioning, etc. This is simply an update to the CSS and removing excess HTML divs. 
</p>

<p>
The last thing I needed to do was solve the problem of having the component load without being visible on the page itself.  Due to how the game itself renders, it renders when the page renders and calculates frames from there.  However, if it's not on the screen, it doesn't seem to start the initial frame and therefore it can't start.  The solution was to update the index.html javascript to read
</p>

<p>
window.SetFocusToElement = () => {
        document.getElementById('app').focus({
            preventScroll: true
            });
        };
</p>

<p>
After combining these techniques I was able to get the game to load as you should see below!  This was an awesome exercise and I am ecstatic as to the ease of use.  I'm definitely planning on continuing to work with WebAssembly in the future and who knows, maybe that will be my next rework of the website. 
</p>

<p>
Blazor will be officially launched in May of 2020.
</p>

<p>
(Click the game to start playing)
</p>

<link href="/assets/css/site.css" rel="stylesheet" type="text/css" />

<app id="app"><span style="color:green">Loading...</span></app>

<script src="/_framework/blazor.webassembly.js"></script>

<script>
    window.SetFocusToElement = () => {
        // var x = window.scrollX, y = window.scrollY;
            document.getElementById('app').focus({
preventScroll: true
});
            // window.scrollTo(x, y);
        };
</script>
