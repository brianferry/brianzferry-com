---
title: 'Using the Giantbomb API'
date: "2019-05-10"
banner_image: "/assets/images/giantbomb-logo.png"
banner_alt: "Giantbomb logo" 
snippet: "\"You can't beat 100%\" - Jeff Gerstmann"
author_name: "Brian Ferry"
layout: post.html
tags: ['Guides', 'Technology', 'Video Games']
---
<div>
<section class="line-numbers">
<b>WHAT IS THE GIANTBOMB API?</b>
<p>
The GiantBomb API is a video game database API. The API hooks into the GiantBomb database and is able to pull back video game related information such as, but not limited to:
</p>

<b>Video Game Information</b>
<ul>
  <li>Name</li>
  <li>Release Date(s)</li>
  <li>System(s)</li>
  <li>Etc.</li>
  <li>Character Information</li>
  <li>Company Information</li>
  <li>GiantBomb Videos</li>
  <li>Etc.</li>
</ul>
More information can be found at The Official GiantBomb API Page.
 
<b>SOFTWARE USED</b>
<p>
Postman API Development Environment
 </p>
<b>HOW DO I GET MY API KEY?</b>
<p>
You must have a user account on GiantBomb.com in order to use this API.

Once you have a user account, go to https://www.giantbomb.com/api/ and sign up for your unique API key.
</p>
 
<b>HOW DO I USE THE GIANTBOMB API?</b>
<p>
Official Documentation Here

To use the GiantBomb API you will need to take your API key and plug it into any requests you make to the API.

To make a request for a character, you must first know their ID.

You can search for the ID by making a GET request, such as the following for Crash Bandicoot https://www.giantbomb.com/api/search/?api_key=[API_KEY]&query=Crash Bandicoot
</p>
 
<p>
Breaking this down, we have

 </p>
<ul>
<li>api/search/ - this let's the API know that we want to make a search on the database</li>

<li>?api_key - this let's the API know who is making the request and to authorize the request </li>

<li>&query - this is our search term that we're using against the database.</li>
</ul>
Since we're looking for a character we can also add on the following query string
<ul>
<li>&resources - this tells the API what type of resource we're looking for</li>

</ul>
<p>
For ALL examples in this text we will be using the format=json tag to make the results handed back to us in JSON format. You can also specify JSONP or XML if those are your preferred formats.
</p>
 
<p>
Since we're also just looking for the ID of the character to get more information we can use
</p>
 
<ul>
<li>&field_list - This tells the API what fields to give back (comma separated) </li>
</ul>
<p>
Meaning the final URL would be https://www.giantbomb.com/api/search/?api_key=[API_KEY]&query=Crash Bandicoot&resources=character&field_list=id,name

From this query we will return a result that looks like this
</p>
<pre class="line-numbers" data-start="0">
<code class="language-json">{ 
  "error": "OK",
  "limit": 10,
  "offset": 0,
  "number_of_page_results": 8,
  "number_of_total_results": 8,
  "status_code": 1,
  "results": [{
    "id": 368,
    "name": "Crash Bandicoot",
    "resource_type": "character"
  }, {
    "id": 6794,
    "name": "Crash",
    "resource_type": "character"
  }, {
    "id": 15998,
    "name": "Crash",
    "resource_type": "character"
  }, {
    "id": 3982,
    "name": "Evil Crash",
    "resource_type": "character"
  }, {
    "id": 1153,
    "name": "Coco Bandicoot",
    "resource_type": "character"
  }, {
    "id": 1234,
    "name": "Crunch Bandicoot",
    "resource_type": "character"
  }, {
    "id": 1179,
    "name": "Crash Man",
    "resource_type": "character"
  }, {
    "id": 14582,
    "name": "Fake Crash",
    "resource_type": "character"
  }],
  "version": "1.0"
}
</code>
</pre>
As we can see from this result the item we're interested in is the first one result. The ID for this result is 368.
<p>
Therefore, we can call the characters information (limiting it to just the name & games here) from the following API call
</p>
https://www.giantbomb.com/api/character/368/?api_key=[API_KEY]&fields=name,games which gives us a result of
<pre class="line-numbers" data-start="0">
<code class="language-json">{
  "error": "OK",
  "limit": 1,
  "offset": 0,
  "number_of_page_results": 1,
  "number_of_total_results": 1,
  "status_code": 1,
  "results": {
    "games": [{
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-71298/",
      "id": 71298,
      "name": "Crash Team Racing: Nitro Fueled",
      "site_detail_url": "https://www.giantbomb.com/crash-team-racing-nitro-fueled/3030-71298/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-57213/",
      "id": 57213,
      "name": "Crash Bandicoot N. Sane Trilogy",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-n-sane-trilogy/3030-57213/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-54071/",
      "id": 54071,
      "name": "Skylanders Imaginators",
      "site_detail_url": "https://www.giantbomb.com/skylanders-imaginators/3030-54071/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-44507/",
      "id": 44507,
      "name": "Uncharted 4: A Thief's End",
      "site_detail_url": "https://www.giantbomb.com/uncharted-4-a-thiefs-end/3030-44507/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-44418/",
      "id": 44418,
      "name": "PocketStation for PlayStation Vita",
      "site_detail_url": "https://www.giantbomb.com/pocketstation-for-playstation-vita/3030-44418/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-32225/",
      "id": 32225,
      "name": "Crash Bandicoot Nitro Kart 2",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-nitro-kart-2/3030-32225/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-20817/",
      "id": 20817,
      "name": "Crash: Mind Over Mutant",
      "site_detail_url": "https://www.giantbomb.com/crash-mind-over-mutant/3030-20817/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-21205/",
      "id": 21205,
      "name": "Crash Bandicoot Nitro Kart 3D",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-nitro-kart-3d/3030-21205/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-7670/",
      "id": 7670,
      "name": "Crash of the Titans",
      "site_detail_url": "https://www.giantbomb.com/crash-of-the-titans/3030-7670/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-21901/",
      "id": 21901,
      "name": "Crash Boom, Bang!",
      "site_detail_url": "https://www.giantbomb.com/crash-boom-bang/3030-21901/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-58384/",
      "id": 58384,
      "name": "Crash & Spyro Super Pack Volume 2",
      "site_detail_url": "https://www.giantbomb.com/crash-spyro-super-pack-volume-2/3030-58384/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-58383/",
      "id": 58383,
      "name": "Crash & Spyro Super Pack Volume 1",
      "site_detail_url": "https://www.giantbomb.com/crash-spyro-super-pack-volume-1/3030-58383/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-35406/",
      "id": 35406,
      "name": "Crash & Spyro Superpack",
      "site_detail_url": "https://www.giantbomb.com/crash-spyro-superpack/3030-35406/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-19795/",
      "id": 19795,
      "name": "Crash SuperPack",
      "site_detail_url": "https://www.giantbomb.com/crash-superpack/3030-19795/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-21405/",
      "id": 21405,
      "name": "Crash Tag Team Racing",
      "site_detail_url": "https://www.giantbomb.com/crash-tag-team-racing/3030-21405/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-14790/",
      "id": 14790,
      "name": "Crash Twinsanity",
      "site_detail_url": "https://www.giantbomb.com/crash-twinsanity/3030-14790/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-732/",
      "id": 732,
      "name": "Crash Bandicoot Purple: Ripto's Rampage",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-purple-riptos-rampage/3030-732/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-15249/",
      "id": 15249,
      "name": "Spyro Orange: The Cortex Conspiracy",
      "site_detail_url": "https://www.giantbomb.com/spyro-orange-the-cortex-conspiracy/3030-15249/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-2278/",
      "id": 2278,
      "name": "Crash Nitro Kart",
      "site_detail_url": "https://www.giantbomb.com/crash-nitro-kart/3030-2278/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-355/",
      "id": 355,
      "name": "Crash Bandicoot 2: N-Tranced",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-2-n-tranced/3030-355/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-7659/",
      "id": 7659,
      "name": "Crash Bandicoot: The Huge Adventure",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-the-huge-adventure/3030-7659/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-68/",
      "id": 68,
      "name": "Crash Bandicoot: The Wrath of Cortex",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-the-wrath-of-cortex/3030-68/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-15952/",
      "id": 15952,
      "name": "Crash Bash",
      "site_detail_url": "https://www.giantbomb.com/crash-bash/3030-15952/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-6196/",
      "id": 6196,
      "name": "Crash Team Racing",
      "site_detail_url": "https://www.giantbomb.com/crash-team-racing/3030-6196/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-12909/",
      "id": 12909,
      "name": "Crash Bandicoot: Warped",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-warped/3030-12909/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-8660/",
      "id": 8660,
      "name": "Crash Bandicoot 2: Cortex Strikes Back",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot-2-cortex-strikes-back/3030-8660/"
    }, {
      "api_detail_url": "https://www.giantbomb.com/api/game/3030-7380/",
      "id": 7380,
      "name": "Crash Bandicoot",
      "site_detail_url": "https://www.giantbomb.com/crash-bandicoot/3030-7380/"
    }],
    "name": "Crash Bandicoot"
  },
  "version": "0"
}
</code>
</pre>
</section>
</div>