---
title: 'SNES Classic Modding Notes'
date: "2019-05-19"
banner_image: "/assets/images/super-mario-1508624_1280.jpg"
banner_alt: "Super Mario" 
snippet: "Personal notes for hardware, software, etc that I had while modding my SNES classic."
author_name: "Brian Ferry"
layout: post.html
tags: ['Guides', 'Technology', 'Video Games']
---
<div>
<b>SOFTWARE USED</b>
<ul>
    <li>
        <a href="https://hakchiresources.com/2018/11/26/hakchi2-ce-community-edition-v3-5-2/" target="_blank">Hakchi CE</a>
    </li>
</ul>
<b>ITEMS PURCHASED</b>
<ul>
    <li>
        <a target="_blank" href="https://www.amazon.com/dp/B00WO4CDFK/ref=sspa_dk_detail_0?psc=1&pd_rd_i=B00WO4CDFK&pd_rd_w=WIiMC&pf_rd_p=46cdcfa7-b302-4268-b799-8f7d8cb5008b&pd_rd_wg=ukw48&pf_rd_r=Z2043M0EGX68044PY509&pd_rd_r=e65d55cf-61d7-11e9-953e-dd131782f63a">USB OTG Adapter</a>
    </li>
    <li>
        <a target="_blank" href="https://www.amazon.com/SanDisk-Ultra-128GB-SDCZ43-128G-G46-Version/dp/B00YFI1EBC">USB Flash Drive</a>
    </li>
</ul>
<b>GUIDES USED</b>
<ul>
    <li>
        <a target="_blank" href="https://snesclassicmods.com/how-to-add-more-games-to-snes-classic-mini/">SNES Classic Mods - How To Add More Games To SNES Classic Mini</a>
    </li>
    <li>
        <a target="_blank" href="https://snesclassicmods.com/add-more-storage-with-external-usb-hard-drive-mod-for-snes-classic-mini/">SNES Classic Mods - Add More Storage With External USB Hard Drive</a>
    </li>
</ul>
<b>SPECIAL NOTES</b>
<ul>
    <li>When I modded my SNES Classic I was not able to get the kernel to flash or USB support to work on the non-CE version of Hakchi so I had to use the Hakchi CE version.</li>
    <li>Make sure that the flash drive is formatted to NTFS and NOT exFAT and that you use the export tool when adding games to the device.</li>
    <li>Keep game to folder ration around 20-30 games per folder for the UI. More or less than 20-30 will cause UI glitches with the SNES Classic.</li>
</ul>
</div>