---
title: 'Project Cyclops'
date: "2019-05-22"
banner_image: "/assets/images/ancient-asian-blurred-background-1344472.jpg"
banner_alt: "Buddha" 
snippet: "\"DO NOT DWELL IN THE PAST, DO NOT DREAM OF THE FUTURE, CONCENTRATE THE MIND ON THE PRESENT MOMENT.\" - BUDDHA"
author_name: "Brian Ferry"
layout: post.html
tags: ['Guides', 'Technology']
---
<div>
<b>WHAT IS PROJECT CYCLOPS?</b>
<p>
CGP Grey Blog on Project Cyclops.

The idea behind it is simple, with everything trying to get our attention all the time it is important to be mindful of what is getting your attention and how meaningful it is to you.

In order to do this I am cutting out certain parts of the internet I have determined to be a negative value for my attention.

Starting this week and continuing indefinitely I will be cutting out the following for personal use:
</p>
<ul>
<li>Reddit</li>
<li>Twitter</li>
<li>Podcasts</li>
<li>News Aggregate Sites (Hackernews, etc)</li>
</ul>

I will add more as I see fit and update on my progress accordingly.
</div>