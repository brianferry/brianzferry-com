const Metalsmith = require('metalsmith')
const markdown = require('metalsmith-markdown')
const layouts = require('metalsmith-layouts')
const permalinks = require('metalsmith-permalinks')
const collections = require('metalsmith-collections')
const dateFormatter = require('metalsmith-date-formatter')
const pagination = require('metalsmith-pagination')
const handlebars = require('handlebars')
const express = require('express')
express.static.mime.define({'application/wasm': ['wasm']});
const app = express()
const port = 4000

handlebars.registerHelper('safeString', function (content) {
  return new handlebars.SafeString(content);
})

// Serve the folder "build"
app.use(express.static('build'))

// Allows us to trigger the rebuild using a simple
// GET request from everywhere if the express server
// is running.
app.get('/rebuild', (req, res) => {
  res.write('Rebuild Started\n');

  build(() => {
    res.write('Rebuild done');
    res.end()
  })
})

// Listen to Port 4000 and start initial build
app.listen(port, () => {
  build(() => { console.log('Finished Building'); console.log('Open on http://localhost:' + port) }) // <-- a new "build" function
})

const build = (finish) => {
  Metalsmith(__dirname)
    .metadata({
      navigation_title: "Brian Ferry's Website",
      author_name: "Brian Ferry",
      author_blurb: "Software Engineer | Raleigh, NC",
      description: "Software Engineer living in my home state of North Carolina.",
      generator: "Metalsmith",
      generator_url: "https://brianzferry.com"
    })
    .use(collections({
      "posts": {
        pattern: "posts/*.md",
        sortBy: "date",
        reverse: true
      }
    }))
    .source('./src')
    .destination('./build')
    .clean(false)
    .use(pagination({
      'collections.posts': {
        perPage: 5,
        layout: "home.html",
        first: "index.html",
        path: 'posts/:num/index.html',
        filter: function (page) {
          return !page.private
        }
      }
    }))
    .use(dateFormatter({
      dates: [
        {
          key: 'date',
          format: "MMMM DD, YYYY"
        }
      ]
    }))
    .use(markdown())
    .use(permalinks({
      relative: false
    }))
    .use(layouts({
      engine: 'handlebars',
      partials: 'partials'
    }))
    .build(function (err, files) {
      if (err) { throw err }
      finish()
    })
}